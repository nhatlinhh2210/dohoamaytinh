﻿#include "Dependencies/glew/glew.h"
#include "Dependencies/freeglut/freeglut.h"
#include <math.h>

//hang qui uoc trang thai di chuyen cua tay va chan
const char BACKWARD_STATE = 0;
const char FORWARD_STATE = 1;
//index qui uoc cho mang (goc xoay cua tay va chan)
const char LEFT = 0;
const char RIGHT = 1;
/*trang thai di chuyen hien tai cua tay va chan (BACKWARD_STATE/FORWARD_STATE)*/
char legStates[2];
char armStates[2];

//goc xoay hien tai cua tay va chan
float legAngles[2];
float armAngles[2];

//goc xoay hien tai cua toan bo robot
float angle;

//cac huong di chuyen co the
const char MOVE_LEFT = 0;
const char MOVE_RIGHT = 1;
const char MOVE_UP = 3;
const char MOVE_DOWN = 4;

//goc xoay robot tuong ung khi su dung cac phim dieu khien
const int TURN_LEFT = -90;
const int TURN_RIGHT = 90;
const int TURN_UP = 180;
const int TURN_DOWN = 0;

const int GROUND_SIZE = 250; // kich thuoc cua mat san
const float FLOOR_HEIGHT = -2; //do cao cua mat san
const int CHECK_SIZE = 5; // kich thuoc cua moi o vuong

int moveDirection; //huong di chuyen hien tai
float moveX; // vi tri di chuyen den theo truc x
float moveZ;	//vi tri di chuyen den theo truc z
float theta;	//goc xoay cua camera (tinh toa do x va z)
float y;	//toa do y cua camera
float dTheta;	//muc tang/giam theta khi dieu khien
float dy;	//muc tang/giam y khi dieu khien

//khai bao ham
void DrawCube(float xPos, float yPos, float zPos);
void DrawArm(float xPos, float yPos, float zPos);
void DrawHead(float xPos, float yPos, float zPos);
void DrawTorso(float xPos, float yPos, float zPos);
void DrawLeg(float xPos, float yPos, float zPos);
void DrawRobot(float xPos, float yPos, float zPos);
void Prepare();
void Display();
void Init();
void Reshape(int Width, int Height);
void Idle();

void DrawGround();
void SettingCamera(float theta, float y);
void Keyboard(unsigned char key, int, int);
void Special(int key, int, int);

//ham DrawGround de ve mat san
void DrawGround()
{

	/*---------làn đường bên trái---------*/
	glPushMatrix();

	glTranslatef(0.0f, 0.0f, 0.0f);

	glColor3f(0.1, 0.1, 1.0);

	glBegin(GL_QUADS);

	glNormal3d(0, 1, 0);
	glVertex3f(-7.5, 0, 30);		//tọa độ góc dưới trái
	glVertex3f(-7.5, 0, -100);		//tọa độ góc trên trái
	glVertex3f(-2.5, 0, -100);		//tọa độ góc trên phải
	glVertex3f(-2.5, 0, 30);		//tọa độ góc dưới phải

	glEnd();
	glPopMatrix();

	/*----------làn đường giữa-----------*/
	glPushMatrix();

	glTranslatef(0.0f, 0.0f, 0.0f);

	glColor3f(0.2, 0.2, 1.0);

	glBegin(GL_QUADS);

	glNormal3d(0, 1, 0);
	glVertex3f(-2.5, 0, 30);		//tọa độ góc dưới trái
	glVertex3f(-2.5, 0, -100);		//tọa độ góc trên trái
	glVertex3f(2.5, 0, -100);		//tọa độ góc trên phải
	glVertex3f(2.5, 0, 30);			//tọa độ góc dưới phải

	glEnd();

	glPopMatrix();

	/*---------làn đường bên phải-----------*/
	glPushMatrix();

	glTranslatef(0.0f, 0.0f, 0.0f);

	glColor3f(0.3, 0.3, 1.0);

	glBegin(GL_QUADS);

	glNormal3d(0, 1, 0);
	glVertex3f(2.5, 0, 30);			//tọa độ góc dưới trái
	glVertex3f(2.5, 0, -100);		//tọa độ góc trên trái
	glVertex3f(7.5, 0, -100);		//tọa độ góc trên phải
	glVertex3f(7.5, 0, 30);			//tọa độ góc dưới phải

	glEnd();

	glPopMatrix();

	//for (x = -GROUND_SIZE; x < GROUND_SIZE; x += CHECK_SIZE)
	//{
	//	for (z = -GROUND_SIZE; z < GROUND_SIZE; z += CHECK_SIZE)
	//	{
	//		if (counter % 2 == 0)
	//			glColor3f(0.2, 0.2, 1.0);	//to mau xanh
	//		else
	//			glColor3f(1.0, 1.0, 1.0);	//to mau trang

	//		glBegin(GL_QUADS);
	//		glNormal3d(0, 1, 0);
	//		glVertex3f(x, FLOOR_HEIGHT, z);
	//		glVertex3f(x, FLOOR_HEIGHT, z + CHECK_SIZE);
	//		glVertex3f(x - CHECK_SIZE, FLOOR_HEIGHT, z + CHECK_SIZE);
	//		glVertex3f(x - CHECK_SIZE, FLOOR_HEIGHT, z);
	//		glEnd();
	//		counter++;
	//	}
	//	counter++;
	//}
}

//ham DrawCube de ve hinh lap phuong tai vi tri chi dinh
void DrawCube(float xPos, float yPos, float zPos)
{
	glPushMatrix();/*luu trang thai bien doi hien tai vao stack*/
	glTranslatef(xPos, yPos, zPos);
	glBegin(GL_QUADS);
	//ve mat tren
	glNormal3d(0, 1, 0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	//ve mat truoc
	glNormal3d(0, 0, 1);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	//ve mat ben phai
	glNormal3d(1, 0, 0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);
	glVertex3f(0.0f, 0.0f, -1.0f);
	//ve mat ben trai
	glNormal3d(-1, 0, 0);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	//ve mat duoi
	glNormal3d(0, -1, 0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	//ve mat sau
	glNormal3d(0, 0, -1);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);
	glEnd();
	glPopMatrix();//quay lai trang thai bien doi da luu
}
//ham DrawArm de ve canh tay (hinh hop chu nhat) tai vi tri chi dinh
void DrawArm(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);//to mau do
	glTranslatef(xPos, yPos, zPos);
	glScalef(1.0f, 4.0f, 1.0f);		//kich thuoc 1x4x1
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
}
//ham DrawHead de ve dau(hinh hop chu nhat) tai vi tri chi dinh
void DrawHead(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);	//to mau trang
	glTranslatef(xPos, yPos, zPos);
	glScalef(2.0f, 2.0f, 2.0f);		//kich thuoc 2x2x2
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
}

//ham DrawTorso de ve chan(hinh hop chu nhat) tai vi tri chi dinh
void DrawTorso(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(0.0f, 0.0f, 1.0f);	//to mau xanh
	glTranslatef(xPos, yPos, zPos);
	glScalef(3.0f, 5.0f, 2.0f);		//kich thuoc 3x5x2
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
}

//ham DrawLeg de ve chan(hinh hop chu nhat) tai vi tri chi dinh
void DrawLeg(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 0.0f);	//to mau vang
	glTranslatef(xPos, yPos, zPos);
	glScalef(1.0f, 5.0f, 1.0f);		//kich thuoc 1x5x1
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
}

//ham DrawRobot de ve toan bo robot tai vi tri chi dinh
void DrawRobot(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glTranslatef(xPos, yPos, zPos);		//toa do ve robot
	//ve phan dau va phan than
	DrawHead(1.0f, 2.0f, 0.0f);
	DrawTorso(1.5f, 0.0f, 0.0f);
	/*di chuyen canh tay trai va xoay canh tay de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngles[LEFT], 1.0f, 0.0f, 0.0f);
	DrawArm(2.5f, 0.0f, -0.5f);
	glPopMatrix();
	/*di chuyen canh tay phai va xoay canh tay de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngles[RIGHT], 1.0f, 0.0f, 0.0f);
	DrawArm(-1.5f, 0.0f, -0.5f);
	glPopMatrix();
	/*di chuyen chan trai va xoay chan de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngles[LEFT], 1.0f, 0.0f, 0.0f);
	DrawLeg(-0.5f, -5.0f, -0.5f);
	glPopMatrix();
	/*di chuyen chan phai va xoay chan de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngles[RIGHT], 1.0f, 0.0f, 0.0f);
	DrawLeg(1.5f, -5.0f, -0.5f);
	glPopMatrix();
	glPopMatrix();
}

void DrawRobot2(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glTranslatef(xPos, yPos, zPos);		//toa do ve robot
	//ve phan dau va phan than
	DrawHead(1.0f, 2.0f, 0.0f);
	DrawTorso(1.5f, 0.0f, 0.0f);
	/*di chuyen canh tay trai va xoay canh tay de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngles[LEFT], 1.0f, 0.0f, 0.0f);
	DrawArm(2.5f, 0.0f, -0.5f);
	glPopMatrix();
	/*di chuyen canh tay phai va xoay canh tay de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngles[RIGHT], 1.0f, 0.0f, 0.0f);
	DrawArm(-1.5f, 0.0f, -0.5f);
	glPopMatrix();
	/*di chuyen chan trai va xoay chan de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngles[LEFT], 1.0f, 0.0f, 0.0f);
	DrawLeg(-0.5f, -5.0f, -0.5f);
	glPopMatrix();
	/*di chuyen chan phai va xoay chan de tao hieu ung "dang buoc di"*/
	glPushMatrix();
	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngles[RIGHT], 1.0f, 0.0f, 0.0f);
	DrawLeg(1.5f, -5.0f, -0.5f);
	glPopMatrix();
	glPopMatrix();
}

//ham Prepare de tinh toan cac goc xoay cua tay va chan
void Prepare()
{
	/*neu tay/chan dang di chuyen ve phia truoc thi tang
	goc xoay, nguoc lai (di chuyen ve phia sau) thi giam goc xoay. */
	for (char side = 0; side < 2; side++) {
		//goc xoay cho tay
		if (armStates[side] == FORWARD_STATE)
			armAngles[side] += 0.1f;
		else
			armAngles[side] -= 0.1f;

		//thay doi trang thai neu goc xoay vuot qua gia tri cho phep
		if (armAngles[side] >= 15.0f)
			armStates[side] = BACKWARD_STATE;
		else if (armAngles[side] <= -15.0f)
			armStates[side] = FORWARD_STATE;

		//goc xoay cho chan
		if (legStates[side] == FORWARD_STATE)
			legAngles[side] += 0.1f;
		else
			legAngles[side] -= 0.1f;

		//thay doi trang thai neu goc xoay vuot qua gia tri cho phep
		if (legAngles[side] >= 15.0f)
			legStates[side] = BACKWARD_STATE;
		else if (legAngles[side] <= -15.0f)
			legStates[side] = FORWARD_STATE;
	}
	switch (moveDirection)
	{
	case MOVE_LEFT:
		moveX = moveX - 0.015f; break;
	case MOVE_RIGHT:
		moveX = moveX + 0.015f; break;
	case MOVE_UP:
		moveZ = moveZ - 0.015f; break;
	case MOVE_DOWN:
		moveZ = moveZ + 0.015f; break;
	}
}

//ham Display de dung hinh
void Display()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//angle = angle + 0.05f;	//tang goc xoay cua robot
	//if (angle >= 360.0f)	//neu xoay du vong, reset goc xoay
	//	angle = 0.0f;

	glLoadIdentity();
	SettingCamera(theta, y);


	//Ve robot 1
	glPushMatrix();
	//xoay robot quanh truc y
	//glRotatef(angle, 0.0f, 1.0f, 0.0f);
	//Prepare();
	//DrawRobot(0.0f, 0.0f, 0.0f);	//ve robot

	glTranslatef(5.0f, 10.0f, 26.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	Prepare();
	DrawRobot(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	// Ve robot 2
	glPushMatrix();
	glTranslatef(-5.0f, 10.0f, 26.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	Prepare();
	DrawRobot2(0.0f, 0.0f, 0.0f);
	glPopMatrix();

	//Ve duong chay
	glPushMatrix();
	DrawGround();
	glPopMatrix();

	glFlush();
	glutSwapBuffers();
}

//ham Init de thiet lap anh sang moi truong va khoi tao gia tri mac dinh cho robot
void Init()
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//mau anh sang ambient
	GLfloat ambientLight[] = { 0.2f,0.2,0.2f,1.0f };
	//mau anh sang diffuse
	GLfloat diffuseLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	//vi tri nguon sang
	GLfloat lightPos[] = { 25.0f, 25.0f, 25.0f, 0.0 };
	//huong chieu sang
	GLfloat spotDir[] = { 0.0, 0.0, 0.0, 0.0 };

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spotDir);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//gan gia tri mac dinh ban dau cho robot
	angle = 0.0f;
	armAngles[LEFT] = 0.0;
	armAngles[RIGHT] = 0.0;
	legAngles[LEFT] = 0.0;
	legAngles[RIGHT] = 0.0;
	armStates[LEFT] = FORWARD_STATE;
	armStates[RIGHT] = BACKWARD_STATE;
	legStates[LEFT] = FORWARD_STATE;
	legStates[RIGHT] = BACKWARD_STATE;

	moveDirection = MOVE_LEFT;
	angle = TURN_LEFT;
	moveX = 0.0f;
	moveZ = 0.0f;

	//gan gia tri mac dinh ban dau cho camera
	theta = 0.0f;
	y = 10.0f;
	dTheta = 0.04f;
	dy = 1.0f;
}

//ham Reshape de thiet lap khung hinh, phep chieu, va camera
void Reshape(int Width, int Height)
{
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (GLfloat)Width / (GLfloat)Height, 1.0, 200.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(5.0, 5.0, 50.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

//ham Idle de yeu cau ve lai man hinh lien tuc
void Idle()
{
	glutPostRedisplay();
}

//ham SettingCamera de thiet lap camera
void SettingCamera(float theta, float y)
{
	gluLookAt(50 * sin(theta), y, 50 * cos(theta), 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

//ham Keyboard xu ly su kien tu ban phim (thay doi vi tri camera).
void Keyboard(unsigned char key, int, int)
{
	switch (key)
	{
	case 'a': theta -= dTheta; break;
	case 'd': theta += dTheta; break;
	case 'w': y += dy; break;
	case 's': if (y > dy)	y -= dy; break;
	default:
		break;
	}
	glutPostRedisplay();
}

//ham Special xu ly cac phim len, xuong, trai, phai
void Special(int key, int, int)
{
	switch (key)
	{
	case GLUT_KEY_LEFT:
		moveDirection = MOVE_LEFT;
		angle = TURN_LEFT;
		break;
	case GLUT_KEY_RIGHT:
		moveDirection = MOVE_RIGHT;
		angle = TURN_RIGHT;
		break;
	case GLUT_KEY_UP:
		moveDirection = MOVE_UP;
		angle = TURN_UP;
		break;
	case GLUT_KEY_DOWN:
		moveDirection = MOVE_DOWN;
		angle = TURN_DOWN;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

//ham main
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(80, 80);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Robot_02");
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutIdleFunc(Idle);
	glutSpecialFunc(Special);
	glutKeyboardFunc(Keyboard);
	Init();
	glutMainLoop();
	return 0;
}